#!/usr/bin/python3
# -*- coding: utf-8 -*-

# dearpygio API Docs
# https://dearpygui.readthedocs.io/en/latest/tutorials/first-steps.html

import dearpygui.dearpygui as dpg

button_run = button_exit = 0

dpg.create_context()
dpg.create_viewport(title='My first app', width=800, height=800, resizable= False)

with dpg.font_registry():
    with dpg.font("Ubuntu.ttf",20) as font1:
        dpg.add_font_range(0x0370, 0x03FF)
    with dpg.font("Ubuntu.ttf",28) as font2:
        dpg.add_font_range(0x0370, 0x03FF)

#dpg.show_font_manager()

def callback(sender, app_data):
    print(f"sender is: {sender}")
    print(f"app_data is: {app_data}")
    if (sender==button_exit):
        dpg.destroy_context()
    else:
        print('button_run')

    #dpg.set_value(textInput0, app_data['current_path'])
    #print(dpg.get_value(listbox0))
    
with dpg.window(label="", width=700, height=700, no_move=True, no_title_bar=True, no_resize=True, pos=[50, 50]):
    label0 = dpg.add_text('Ετικέτα κειμένου:')
    listbox0 = dpg.add_listbox(['Επιλογή 0', 'Επιλογή 1','Επιλογή 2'])
    spacer0 = dpg.add_text("")
    #dpg.add_file_dialog(directory_selector=True, show=False, width=600, height=400, callback=callback, tag="file_dialog_id", modal=True)
    #dpg.add_button(label="Επιλογή φακέλου", callback=lambda: dpg.show_item("file_dialog_id"))
    label1 = dpg.add_text("Πλαίσιο εισαγωγής κειμένου")
    textInput0 = dpg.add_input_text(default_value="Πλαίσιο κειμένου", readonly=False)
    spacer1 = dpg.add_text("")
    with dpg.group(horizontal=True) as group2:
        button_run  = dpg.add_button(label="Εκτέλεση", width=200, callback=callback)
        button_exit  = dpg.add_button(label="Έξοδος", width=200, callback=callback)
    
    dpg.bind_font(font1)
    dpg.bind_item_font(label0, font2)
    dpg.bind_item_font(spacer0, font2)
    dpg.bind_item_font(spacer1, font2)
    dpg.bind_item_font(label1, font2)
    

dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()